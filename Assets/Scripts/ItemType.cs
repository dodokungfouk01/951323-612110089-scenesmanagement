﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType{
    SPHERE_DROP_ITEM,
    CUBE_OBSTACLE,
    CAPSULE_OBSTACLE,
    CYLINDER_OBSTACLE,
    KRATANG_OBSTACLE,
    OOG_OBSTACLE,
    
}
